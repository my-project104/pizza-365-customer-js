
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

var gSizePizza = {
    diaChi: "",
    duongKinh: "",
    email: "",
    hoTen: "",
    idLoaiNuocUong: "",
    idVourcher: "",
    kichCo: "",
    loaiPizza: "",
    loiNhan: "",
    salad: "",
    soLuongNuoc: "",
    sosoDienThoai: "",
    suon: "",
    thanhTien: ""
};
var gVoucher = { maVoucher: "", phanTramGiamGia: 0 };
var gOrderId = "";
var gId = "";

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    // sự kiện chọn drink
    $("#select-drink").on("change", function () {
        console.log("Chọn Drink")
        getSelectDrink();
    });
    // sự kiện chọn size S
    $("#btn-chon-S").on("click", function () {
        console.log("Chọn Size S!")
        resetColorBtnSizePiza();
        onBtnSizeSClick();
    });
    // sự kiện chọn size M
    $("#btn-chon-M").on("click", function () {
        console.log("Chọn Size M!")
        resetColorBtnSizePiza();
        onBtnSizeMClick();
    });
    // sự kiện chọn size L
    $("#btn-chon-L").on("click", function () {
        console.log("Chọn Size L!")
        resetColorBtnSizePiza();
        onBtnSizeLClick();
    });
    // sự kiện chọn type pizza 1
    $("#btn-chon-1").on("click", function () {
        console.log("Chọn Pizza loại 1!")
        resetColorBtnTypePizza();
        onBtnType1Click();
    });
    // sự kiện chọn type pizza 2
    $("#btn-chon-2").on("click", function () {
        console.log("Chọn Pizza loại 2!")
        resetColorBtnTypePizza();
        onBtnType2Click();
    });
    // sự kiện chọn type pizza 3
    $("#btn-chon-3").on("click", function () {
        console.log("Chọn Pizza loại 3!")
        resetColorBtnTypePizza();
        onBtnType3Click();
    });
    // sự kiện click nút gửi đơn
    $("#btn-guidonhang").on("click", function () {
        console.log("Gửi đơn!")
        console.log(gSizePizza.thanhTien)
        sentDataOrder(gSizePizza);
    });
    // sự kiện click nút tạo đơn trong modal 1
    $("#btn-taodon").on("click", function () {
        callAPICreateOrder();
        $("#exampleModal").modal("hide");
    });
    // sự kiện click nút OK trong modal 2
    $("#btn-ok").on("click", function () {
        resetFormOrder();
        $("#taodonhangthanhcong").modal("hide");
    });

    // Load tát cả loại nước uống
    callAPIAllDrink();

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

    function onBtnSizeSClick() {
        // hàm đổi màu nút được chọn và gán giá trị tương ứng vào biến toàn cục
        var vBtnS = document.getElementById("btn-chon-S");
        vBtnS.className = "btn btn-success w-100";
        gSizePizza.kichCo = "S"
        gSizePizza.duongKinh = "20cm"
        gSizePizza.suon = "2"
        gSizePizza.salad = "200g"
        gSizePizza.soLuongNuoc = "2"
        gSizePizza.thanhTien = "150000"
        console.log(gSizePizza);
    }
    function onBtnSizeMClick() {
        // hàm đổi màu nút được chọn và gán giá trị tương ứng vào biến toàn cục
        var vBtnM = document.getElementById("btn-chon-M");
        vBtnM.className = "btn btn-success w-100";
        gSizePizza.kichCo = "M"
        gSizePizza.duongKinh = "25cm"
        gSizePizza.suon = "4"
        gSizePizza.salad = "300g"
        gSizePizza.soLuongNuoc = "3"
        gSizePizza.thanhTien = "200000"
        console.log(gSizePizza);
    }
    function onBtnSizeLClick() {
        // hàm đổi màu nút được chọn và gán giá trị tương ứng vào biến toàn cục
        var vBtnL = document.getElementById("btn-chon-L");
        vBtnL.className = "btn btn-success w-100";
        gSizePizza.kichCo = "L"
        gSizePizza.duongKinh = "30cm"
        gSizePizza.suon = "8"
        gSizePizza.salad = "500g"
        gSizePizza.soLuongNuoc = "4"
        gSizePizza.thanhTien = "250000"
        console.log(gSizePizza);
    }
    function onBtnType1Click() {
        // hàm đổi màu nút được chọn và gán giá trị tương ứng vào biến toàn cục
        var vBtn1 = document.getElementById("btn-chon-1");
        vBtn1.className = "btn btn-success w-100";
        gSizePizza.loaiPizza = "OCEAN MANIA"
        console.log(gSizePizza);
    }
    function onBtnType2Click() {
        // hàm đổi màu nút được chọn và gán giá trị tương ứng vào biến toàn cục
        var vBtn2 = document.getElementById("btn-chon-2");
        vBtn2.className = "btn btn-success w-100";
        gSizePizza.loaiPizza = "HAWAIIAN"
        console.log(gSizePizza);
    }
    function onBtnType3Click() {
        // hàm đổi màu nút được chọn và gán giá trị tương ứng vào biến toàn cục
        var vBtn3 = document.getElementById("btn-chon-3");
        vBtn3.className = "btn btn-success w-100";
        gSizePizza.loaiPizza = "CHEESY CHICKEN BACON"
        console.log(gSizePizza);
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // hàm gọi API lấy về thông tin tất cả loại nước uống
    function callAPIAllDrink() {
        // gọi API
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
            type: 'GET',
            dataType: 'json',
            success: function (paramObjectDrink) {
                console.log(paramObjectDrink);
                // Hiển thị dữ liệu lên select drink
                loadSelectDrink(paramObjectDrink)
            }
        });
    }
    // hàm load drink lên select
    function loadSelectDrink(paramObjectDrink) {

        for (var bI = 0; bI < paramObjectDrink.length; bI++) {
            $('#select-drink').append($('<option>', {
                value: paramObjectDrink[bI].maNuocUong,
                text: paramObjectDrink[bI].tenNuocUong
            }));
        }
    }
    // hàm lấy thông tin nước được chọn vào biến toàn cục
    function getSelectDrink() {
        var vSelectDrink = $("#select-drink");
        gSizePizza.idLoaiNuocUong = vSelectDrink.val();
        console.log(gSizePizza.idLoaiNuocUong);
    }
    // hàm reset 3 nút chọn Size S M L thành màu cam lúc đầu
    function resetColorBtnSizePiza() {

        var vBtnS = document.getElementById("btn-chon-S");
        vBtnS.className = "btn btn-warning w-100";
        var vBtnM = document.getElementById("btn-chon-M");
        vBtnM.className = "btn btn-warning w-100";
        var vBtnL = document.getElementById("btn-chon-L");
        vBtnL.className = "btn btn-warning w-100";
    }
    // hàm reser màu 3 nút type piza
    function resetColorBtnTypePizza() {

        var vBtn1 = document.getElementById("btn-chon-1");
        vBtn1.className = "btn btn-warning w-100";
        var vBtn2 = document.getElementById("btn-chon-2");
        vBtn2.className = "btn btn-warning w-100";
        var vBtn3 = document.getElementById("btn-chon-3");
        vBtn3.className = "btn btn-warning w-100";
    }
    // hàm gửi dữ liệu về sever
    function sentDataOrder() {
        // thu thập dữ liệu
        getDataOrder();
        // validate dử liệu
        var vValidateData = validateDataOrder(gSizePizza);
        if (vValidateData) {
            console.log("Đơn hàng đã được tạo thành công!")
            // hiển thị dữ liệu lên modal
            showDataOrderToModal();
        }
    }
    // hàm thu thập dữ liệu nhập vào của khách hàng
    function getDataOrder() {

        gSizePizza.hoTen = $("#inp-fullname").val().trim();
        gSizePizza.email = $("#inp-email").val().trim();
        gSizePizza.soDienThoai = $("#inp-dien-thoai").val().trim();
        gSizePizza.diaChi = $("#inp-dia-chi").val().trim();
        gSizePizza.idVourcher = $("#inp-voucher").val().trim();
        gSizePizza.loiNhan = $("#inp-message").val().trim();
    }
    // hàm validate dữ liệu
    function validateDataOrder(paramDataOrder) {

        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (paramDataOrder.thanhTien == "") {
            alert("Vui lòng chọn Size pizza");
            return false;
        }
        if (paramDataOrder.loaiPizza == "") {
            alert("Vui lòng chọn Menu");
            return false;
        }
        if (paramDataOrder.idLoaiNuocUong == "") {
            alert("Hãy chọn nước uống!");
            return false;
        }
        if (paramDataOrder.hoTen == "") {
            alert("Họ và Tên phải nhập vào");
            return false;
        }
        if (!paramDataOrder.email.match(mailformat)) {
            alert("Email nhập vào không hợp lệ!");
            return false;
        }
        if (paramDataOrder.soDienThoai == "" || isNaN(paramDataOrder.soDienThoai) || paramDataOrder.soDienThoai.length != 10) {
            alert("Số điện thoại phải là số có 10 chữ số!");
            return false;
        }
        if (paramDataOrder.diaChi == "") {
            alert("Địa chỉ phải nhập vào");
            return false;
        }
        checkVoucher(paramDataOrder);
        return true;
    }
    // hàm check voucher
    function checkVoucher(paramDataOrder) {

        var vVoucher = paramDataOrder.idVourcher;
        if (vVoucher == "") {
            alert("Bạn chưa nhập voucher!");
            gVoucher.phanTramGiamGia = 0;
        } else {
            // gọi API
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + vVoucher,
                async: false,
                type: 'GET',
                dataType: 'json',
                success: function (paramObjectVoucher) {
                    console.log(paramObjectVoucher);
                    // gáng giá trị trả về vào biến toàn cục để tiếp tục sử dụng
                    gVoucher.maVoucher = paramObjectVoucher.maVoucher;
                    gVoucher.phanTramGiamGia = parseInt(paramObjectVoucher.phanTramGiamGia)
                    // hiển thị thông báo ra web cho khách hàng.
                    console.log("Giảm giá :" + gVoucher.phanTramGiamGia + "%");
                    alert("Voucher : " + gVoucher.phanTramGiamGia + "%");
                },
                error: function () {
                    // gáng giá trị trả về vào biến toàn cục để tiếp tục sử dụng
                    gVoucher.maVoucher = vVoucher;
                    gVoucher.phanTramGiamGia = 0;
                    // hiển thị thông báo ra web cho khách hàng.
                    console.log("Không tìm thấy voucher");
                    alert("Voucher không tìm thấy!");
                }
            });
        }
    }
    // hàm load thông tin order lên modal
    function showDataOrderToModal() {
        // làm trống modal
        resetFormModal();
        // hiển thị dữ liệu ra modal
        $("#hovaten").val(gSizePizza.hoTen);
        $("#sodienthoai").val(gSizePizza.soDienThoai);
        $("#diachi").val(gSizePizza.diaChi);
        $("#loinhan").val(gSizePizza.loiNhan);
        $("#magiamgia").val(gSizePizza.idVourcher);
        $("#thongtinchitiet").html("Xác nhận: " + gSizePizza.hoTen + " - " + gSizePizza.soDienThoai + " - " + gSizePizza.diaChi + "\n")
            .append("Menu: " + gSizePizza.kichCo + " - sườn nướng: " + gSizePizza.suon + " - nước: " + gSizePizza.soLuongNuoc + "\n")
            .append("Loại pizza: " + gSizePizza.loaiPizza + " Giá: " + gSizePizza.thanhTien + " VNĐ. Mã giảm giá: " + gSizePizza.idVourcher + "\n")
            .append("Phải thanh toán: " + gSizePizza.thanhTien * (1 - parseInt(gVoucher.phanTramGiamGia) / 100) + " VNĐ. (Giảm giá " + parseInt(gVoucher.phanTramGiamGia) + "%)" + "\n")
        $("#exampleModal").modal("show");
    }
    // hàm làm trống modal
    function resetFormModal() {
        $("#hovaten").empty();
        $("#sodienthoai").empty();
        $("#diachi").empty();
        $("#loinhan").empty();
        $("#magiamgia").empty();
        $("#thongtinchitiet").empty();
    };
    // hàm tạo order gửi lên sever
    function callAPICreateOrder() {
        // gọi API
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
            async: false,
            type: 'post',
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(gSizePizza),
            dataType: 'json',
            success: function (paramOrder) {
                console.log(paramOrder);
                gOrderId = paramOrder.orderId;
                gId = paramOrder.id;
                // Hiển thị thông tin cho user
                alert("Đã tạo đơn hàng thành công!");
                console.log(gOrderId);
                $("#madonhang").val(gOrderId);
                $("#taodonhangthanhcong").modal("show");
            },
        });
    }
    // hàm load lại trang web
    function resetFormOrder() {
        location.reload();
    }
});


